import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { QrCodeComponent } from '../../pages/qr-code/qr-code.component';
import { AccessiComponent } from '../../pages/accessi/accessi.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { AuthGuardService } from '../../services/authguard.service';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent, canActivate: [AuthGuardService] },
    { path: 'user-profile',   component: UserProfileComponent, canActivate: [AuthGuardService] },
    { path: 'qr-code',        component: QrCodeComponent, canActivate: [AuthGuardService] },
    { path: 'accessi',        component: AccessiComponent, canActivate: [AuthGuardService] },
    { path: 'tables',         component: TablesComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent }
];
