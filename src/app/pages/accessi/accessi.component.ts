import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// core components

@Component({
  selector: 'app-accessi',
  templateUrl: './accessi.component.html',
  styleUrls: ['./accessi.component.scss']
})
export class AccessiComponent implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;

  constructor(private http: HttpClient) { }

  ngOnInit() {

  }

  submitAccess(){
    let payload = {
      "userId":"pippo"
    }
    //this.getAccessList()
    this.postAccess(payload)
  }

  postAccess(p) {
    let url = environment.mainUrl + environment.access
    let payload = p
    let headers = {
      'Authorization':'Bearer ' + localStorage.getItem("token"),
      'Content-Type': 'application/json'
    }
    this.http.post<any>(url, payload,{headers:headers}).subscribe(data => {
      if(data) {
        console.log(data)
        if(data.code == 200) {
        }
      }
        

    })
  }

  getAccessList() {
    let url = environment.mainUrl + environment.accessList;
    let headers = {
      'Authorization':'Bearer ' + localStorage.getItem("token"),
      'Content-Type': 'application/json'
    }
    let params = {
      formDate:"2020-26-06",
      _id: JSON.parse(localStorage.getItem('company'))._id
    }
    this.http.get<any>(url,{headers:headers,params:params}).subscribe(data => {
      if(data) {
        console.log(data)
        if(data.code == 200) {
          console.log("olé")
        }
      }
    })
  }

}
