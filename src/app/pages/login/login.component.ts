import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(public router: Router, private http: HttpClient) {}  

  @ViewChild('codiceFiscale') codiceFiscale: ElementRef;
  @ViewChild('password') password: ElementRef;

  ngOnInit() {
  }
  ngOnDestroy() {
  }

  onSubmitLogin() {
    console.log("SUBMIT")
    let codiceFiscale = this.codiceFiscale.nativeElement.value;
    let password = this.password.nativeElement.value
    console.log(codiceFiscale)
    console.log(password)

    if(codiceFiscale && password) {
      let payload = {
        "codiceFiscale": codiceFiscale,
        "password": password
      }
      this.signIn(payload)
      // localStorage.setItem('token','ok')
      // this.router.navigate(['/user-profile'])
    }
  }

  signIn(p) {
    let url = environment.mainUrl + environment.signIn
    let payload = p
    this.http.post<any>(url, payload).subscribe(data => {
      if(data) {
        console.log(data)
        if(data.code == 200) {
          localStorage.setItem('token',data.body.jwt)
          localStorage.setItem('company',JSON.stringify(data.body.company))
          this.router.navigate(['/user-profile'])
        }
      }
        

    })
  }
  
}
