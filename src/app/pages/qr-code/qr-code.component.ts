import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { environment } from '../../../environments/environment';
import QrScanner from "qr-scanner";

QrScanner.WORKER_PATH = '/assets/js/qr-scanner-worker.min.js';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.scss']
})
export class QrCodeComponent implements OnInit {

  constructor() { }

  @ViewChild('happyFace') happyFace: ElementRef;
  @ViewChild('sadFace') sadFace: ElementRef;
  @ViewChild('happyFaceMessage') happyFaceMessage: ElementRef;
  @ViewChild('sadFaceMessage') sadFaceMessage: ElementRef;
  @ViewChild('qrVideo') qrVideo: ElementRef;
  @ViewChild('qrVideoContent') qrVideoContent: ElementRef;

  scannedData: {};
  testo:any;
  scanner:any;

  ngOnInit() {
    if(environment.production) {
      console.log("production")
      console.log(environment.mainUrl)
    }
    else{
      console.log("local")
      console.log(environment.mainUrl)
    }
  }

  startscanner(){
    this.hideButtons("scanning")
    this.qrVideoContent.nativeElement.style.display = "block"
    this.scanner = new QrScanner(this.qrVideo.nativeElement, result => this.setResult(result));
    this.scanner.setInversionMode('original');
    this.scanner.start();   
  }

  setResult(result) {
    let js = JSON.parse(result)
    console.log(js)
    
    if(js && js.dati.ammalato) {
      this.sadFaceMessage.nativeElement.innerHTML = "Buongiorno " + js.anagrafica.nome + " " + js.anagrafica.cognome + ", benvenuto!<br />Mi dispiace ma non puoi accedere!"
      this.sadFace.nativeElement.style.display = "block"
      this.stopscanner(false)
    }
    else if(js && !js.dati.ammalato) {
      this.happyFaceMessage.nativeElement.innerHTML = "Buongiorno " + js.anagrafica.nome + " " + js.anagrafica.cognome + ", benvenuto!<br />Puoi accedere!"
      this.happyFace.nativeElement.style.display = "block"
      this.stopscanner(false)
    }
  }

  stopscanner(exit){
    this.qrVideoContent.nativeElement.style.display = "none"
    console.log("Stop scanner")
    if(this.scanner)
      this.scanner.destroy();
    if(!exit) {
      setTimeout(() => {
        this.sadFace.nativeElement.style.display = "none"
        this.happyFace.nativeElement.style.display = "none"
        this.hideButtons("buttons")
      }, 5000)
    }
    else {      
      this.sadFace.nativeElement.style.display = "none"
      this.happyFace.nativeElement.style.display = "none"
      this.hideButtons("buttons")
    }
  }

  hideButtons(visible){
    if(visible == "scanning") {
      document.getElementById("buttons").style.display = "none"
      document.getElementById("scanning").style.display = "block"
      document.getElementById("autocertificazione").style.display = "none"
    }
    else if(visible == "buttons") {
      document.getElementById("buttons").style.display = "block"
      document.getElementById("scanning").style.display = "none"
      document.getElementById("autocertificazione").style.display = "none"
    }
    else if(visible == "autocertificazione") {
      document.getElementById("buttons").style.display = "none"
      document.getElementById("scanning").style.display = "none"
      document.getElementById("autocertificazione").style.display = "block"
    }

  }

}
