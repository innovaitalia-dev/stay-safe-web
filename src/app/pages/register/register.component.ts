import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  
  constructor(public router: Router, private http: HttpClient) {}  

  @ViewChild('codiceFiscale') codiceFiscale: ElementRef;
  @ViewChild('password') password: ElementRef;
  @ViewChild('nome') nome: ElementRef;
  //@ViewChild('sedi') sedi: ElementRef;

  error = ""

  ngOnInit() {
    console.log("register")
  }
  onSubmitRegister() {
    console.log("SUBMIT")
    let codiceFiscale = this.codiceFiscale.nativeElement.value;
    let password = this.password.nativeElement.value
    let nome = this.nome.nativeElement.value
    //let sedi = this.sedi.nativeElement.value
    console.log(codiceFiscale)
    console.log(password)
    console.log(nome)
    //console.log(sedi)

    if(codiceFiscale && password && nome) {
      let aree = [
        {
          "nome": "area 1",
          "posti": 100
        }
      ]
      let sedi = [
        {
          "nome":"sede1",
          "indirizzo": "via 1 città 1",
          "telefono": "telefono 1",
          "aree": aree
        }
      ]

      let payload = {
        "codiceFiscale": codiceFiscale,
        "password": password,
        "nome": nome,
        "sedi": sedi
      }
      this.signUp(payload)
      // localStorage.setItem('token','ok')
      // this.router.navigate(['/user-profile'])
    }
  }

  signUp(p) {
    let url = environment.mainUrl + environment.signUp
    let payload = p
    console.log(p)
    this.http.post<any>(url, payload).subscribe(data => {
      if(data) {
        console.log(data)
        if(data.code == 201)
          this.router.navigate(['/login'])
        else if(data.code == 409)
          this.error = "Attenzione, Company già registrata!"
      }      
    })
  }
}
